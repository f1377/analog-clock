import 'package:analog_clock/screens/clock_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(AnalogClockExample());
}

class AnalogClockExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ClockScreen(),
    );
  }
}
