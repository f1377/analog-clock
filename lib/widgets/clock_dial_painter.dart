import 'dart:math';

import 'package:flutter/material.dart';

enum ClockText { roman, arabic }

class ClockDialPainter extends CustomPainter {
  final ClockText clockText;

  final double hourTickMarkLength = 10.0;
  final double minuteTickMarkLength = 5.0;

  final double hourTickMarkWith = 3.0;
  final double minuteTickMarkWidth = 1.5;

  final Paint tickPaint;
  final TextPainter textPainter;
  final TextStyle textStyle;

  final romanNumeralList = [
    'XII',
    'I',
    'II',
    'III',
    'IV',
    'V',
    'VI',
    'VII',
    'VIII',
    'IX',
    'X',
    'XI'
  ];

  ClockDialPainter({this.clockText: ClockText.roman})
      : tickPaint = Paint(),
        textPainter = TextPainter(
          textAlign: TextAlign.center,
          textDirection: TextDirection.rtl,
        ),
        textStyle = const TextStyle(
          color: Colors.black,
          fontFamily: 'Times New Roman',
          fontSize: 15.0,
        ) {
    tickPaint.color = Colors.blueGrey;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double tickMarkLength;

    final double angle = 2 * pi / 60;
    final radius = size.width / 2;
    canvas.save();

    canvas.translate(radius, radius);
    for (double index = 0; index < 60; index++) {
      tickMarkLength =
          index % 5 == 0 ? hourTickMarkLength : minuteTickMarkLength;
      tickPaint.strokeWidth =
          index % 5 == 0 ? hourTickMarkWith : minuteTickMarkWidth;
      Offset position1 = Offset(0.0, -radius + 20);
      Offset position2 = Offset(0.0, -radius + tickMarkLength);

      canvas.drawLine(position1, position2, tickPaint);

      _drawText(canvas, index, angle, radius);

      canvas.rotate(angle);
    }

    canvas.restore();
  }

  void _drawText(Canvas canvas, double index, double angle, double radius) {
    if (index % 5 == 0) {
      canvas.save();
      canvas.translate(0.0, -radius + 40);
      String romanNumber = '${romanNumeralList[index ~/ 5]}';
      String arabicNumber = index == 0 ? '12' : '${index ~/ 5}';

      textPainter.text = TextSpan(
        text: this.clockText == ClockText.roman ? romanNumber : arabicNumber,
        style: textStyle,
      );

      canvas.rotate(-angle * index);
      textPainter.layout();
      double width = -(textPainter.width / 2);
      double height = -(textPainter.height / 2);

      Offset textPainterOffset = Offset(width, height);
      textPainter.paint(canvas, textPainterOffset);

      canvas.restore();
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
