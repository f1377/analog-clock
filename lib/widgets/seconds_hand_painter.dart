import 'dart:math';

import 'package:flutter/material.dart';

class SecondsHandPainter extends CustomPainter {
  final Paint secondHandPaint;
  final Paint secondHandPointPaint;

  final int seconds;

  SecondsHandPainter({required this.seconds})
      : secondHandPaint = Paint(),
        secondHandPointPaint = Paint() {
    secondHandPaint.color = Colors.red;
    secondHandPaint.style = PaintingStyle.stroke;
    secondHandPaint.strokeWidth = 2.0;
    secondHandPointPaint.color = Colors.red;
    secondHandPointPaint.style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final radius = size.width / 2;

    canvas.save();
    canvas.translate(radius, radius);
    canvas.rotate(2 * pi * this.seconds / 60);

    Path hand = Path();
    hand
      ..moveTo(0.0, -radius)
      ..lineTo(0.0, radius / 4);

    Path pints = Path();
    pints
      ..addOval(Rect.fromCircle(center: Offset(0.0, -radius), radius: 7.0))
      ..addOval(Rect.fromCircle(center: Offset(0.0, 0.0), radius: 5.0));

    canvas.drawPath(hand, secondHandPaint);
    canvas.drawPath(pints, secondHandPointPaint);
    canvas.restore();
  }

  @override
  bool shouldRepaint(SecondsHandPainter oldDelegate) {
    return this.seconds != oldDelegate.seconds;
  }
}
