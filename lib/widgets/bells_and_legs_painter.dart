import 'dart:math';

import 'package:flutter/cupertino.dart';

class BellsAndLegsPainter extends CustomPainter {
  final Paint bellPaint;
  final Paint legPaint;

  BellsAndLegsPainter()
      : bellPaint = Paint(),
        legPaint = Paint() {
    bellPaint.color = const Color(0xFF333333);
    bellPaint.style = PaintingStyle.fill;

    legPaint.color = const Color(0xFF555555);
    legPaint.style = PaintingStyle.stroke;
    legPaint.strokeWidth = 10.0;
    legPaint.strokeCap = StrokeCap.round;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final double radius = size.width / 2;
    canvas.save();

    canvas.translate(radius, radius);

    _drawHandle(radius, canvas);

    canvas.rotate(2 * pi / 12);
    _drawBellAndLeg(radius, canvas);

    canvas.rotate(-4 * pi / 12);
    _drawBellAndLeg(radius, canvas);

    canvas.restore();
  }

  void _drawHandle(double radius, Canvas canvas) {
    Path handle = Path();
    handle
      ..moveTo(-60, -radius - 10)
      ..lineTo(-50, -radius - 50)
      ..lineTo(50, -radius - 50)
      ..lineTo(60, -radius - 10);

    canvas.drawPath(handle, legPaint);
  }

  void _drawBellAndLeg(double radius, Canvas canvas) {
    Path bell = Path();
    bell
      ..moveTo(55, -radius - 5)
      ..lineTo(55, -radius - 5)
      ..quadraticBezierTo(0.0, -radius - 75, -55.0, -radius - 10);

    Path leg = Path();
    leg
      ..addOval(Rect.fromCircle(center: Offset(0.0, -radius - 50), radius: 3.0))
      ..moveTo(0.0, -radius - 50)
      ..lineTo(0.0, radius + 20);

    canvas.drawPath(leg, legPaint);
    canvas.drawPath(bell, bellPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
