import 'dart:async';

import 'package:analog_clock/widgets/hour_hand_painter.dart';
import 'package:analog_clock/widgets/minute_hand_painter.dart';
import 'package:analog_clock/widgets/seconds_hand_painter.dart';
import 'package:flutter/material.dart';

class ClockHands extends StatefulWidget {
  const ClockHands({Key? key}) : super(key: key);

  @override
  _ClockHandsState createState() => _ClockHandsState();
}

class _ClockHandsState extends State<ClockHands> {
  Timer? _timer;
  DateTime? dateTime;

  @override
  void initState() {
    super.initState();
    this.dateTime = DateTime.now();
    this._timer = Timer.periodic(const Duration(seconds: 1), _setTime);
  }

  void _setTime(Timer timer) {
    setState(() {
      this.dateTime = DateTime.now();
    });
  }

  @override
  void dispose() {
    this._timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.0,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(20.0),
        child: Stack(
          fit: StackFit.expand,
          children: [
            CustomPaint(
              painter: HourHandPainter(
                hours: this.dateTime!.hour,
                minutes: this.dateTime!.minute,
              ),
            ),
            CustomPaint(
              painter: MinuteHandPainter(
                minutes: this.dateTime!.minute,
                seconds: this.dateTime!.second,
              ),
            ),
            CustomPaint(
              painter: SecondsHandPainter(
                seconds: this.dateTime!.second,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
