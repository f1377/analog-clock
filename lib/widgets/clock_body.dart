import 'package:analog_clock/widgets/bells_and_legs_painter.dart';
import 'package:analog_clock/widgets/clock_face.dart';
import 'package:flutter/material.dart';

class ClockBody extends StatelessWidget {
  const ClockBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.0,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            child: CustomPaint(
              painter: BellsAndLegsPainter(),
            ),
          ),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.black,
            ),
            child: ClockFace(),
          ),
        ],
      ),
    );
  }
}
